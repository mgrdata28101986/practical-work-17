﻿#include <iostream>
#include <cmath>
using namespace std;

class Vector
{
    private:

        double x, y, z;

    public:

        Vector() : x(0), y(0), z(0) {}
        Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

        void Show()
        {
            cout << "\n" << x << " " << y << " " << z;
        }

        double GetModule()
        {
            return sqrt ( pow ( x , 2 ) + pow ( y , 2 ) + pow ( z , 2 ) );
        }
};

int main()
{
    Vector myVector (100, 12.5, 40);

    myVector.Show();

    cout << "\n" << myVector.GetModule();

    return 0;
}

